package utils

import io.javalin.http.Context
import myViews
import java.io.File

/**
 * Created by Mihael Valentin Berčič
 * on 01/04/2020 at 18:56
 * using IntelliJ IDEA
 */

/**
 * Returns html to the request...
 *
 * @param name View / Template name [index]
 * @param data Variables mapped into an array of pairs (variableName, variableValue)
 */
fun Context.renderView(name: String, vararg data: Pair<String, Any?>) {
    val fileName = if (!name.contains(".html")) name else name.replace(".html", "")
    var html: String = myViews[fileName] ?: ""
    data.forEach { (name, content) -> html = html.replace("@$name", "$content") }
    html(html)
}



/**
 * Recursively finds files with the specified extension...
 *
 * @param dirName Root directory of the search
 * @param type File type wanted to store
 * @return Map of <FileName, FileContent>
 */
private fun findFiles(dirName: String, type: String): HashMap<String, String> = hashMapOf<String, String>().apply {
    val directory = File(dirName)
    directory.listFiles()?.forEach { file ->
        if (file.isDirectory) putAll(findFiles(file.path, type))
        else if (file.extension.contains(type)) put(file.nameWithoutExtension, file.readText())
    }
}

/**
 * Loads templates/views and style from the ./ path
 */
fun loadViews() = findFiles("./", "html")
fun loadStyle() = findFiles("./", "css")

