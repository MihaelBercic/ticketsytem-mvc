package models

/**
 * Created by Mihael Valentin Berčič
 * on 02/04/2020 at 13:13
 * using IntelliJ IDEA
 */
data class UpdateRequestObject(val id: Int, val status: Int)
data class NewRequestObject(val title: String, val description: String)