package models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

/**
 * Created by Mihael Valentin Berčič
 * on 02/04/2020 at 01:58
 * using IntelliJ IDEA
 */

object TicketsTable : Table() {
    val id: Column<Int> = integer("id").autoIncrement()
    val title: Column<String> = varchar("title", 50)
    val description: Column<String> = varchar("name", 100)
    val status: Column<Int> = integer("status").default(0)
    override val primaryKey = PrimaryKey(id)
}

data class Ticket(
        val id: Int,
        val title: String,
        val description: String,
        val status: Int
) {

    @Transient
    private val options = mapOf(0 to "Open", 1 to "Working on", 2 to "Closed")
    private val htmlOptions: String get() = options.map { (i, s) -> "<option value='$i' ${i.isSelectedAttribute} >$s</option>" }.joinToString("")
    private val Int.isSelectedAttribute get() = if (this == status) "selected" else ""

    private val classStatus: String
        get() = when (status) {
            0 -> "open"
            1 -> "working"
            else -> "closed"
        }

    override fun toString(): String = "<li id='ticket-$id' class='ticket $classStatus'>" +
            "                <div class='title'>#$id $title</div>" +
            "                <div class='description'>$description</div>" +
            "                <select onchange='onChange(this, $id)'>$htmlOptions</select>" +
            "                <div class='hint'>Change</div>" +
            "            </li>"

}