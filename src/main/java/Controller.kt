import com.google.gson.Gson
import io.javalin.Javalin
import io.javalin.http.Context
import models.NewRequestObject
import models.Ticket
import models.TicketsTable
import models.UpdateRequestObject
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import utils.loadStyle
import utils.loadViews
import utils.renderView

/**
 * Created by Mihael Valentin Berčič
 * on 01/04/2020 at 18:44
 * using IntelliJ IDEA
 */
// Templates and style
val myViews = loadViews()
val styles = loadStyle()

private val gson = Gson()

class Controller {

    private val app = Javalin.create { it.showJavalinBanner = false }.start(3000)

    fun launch() {

        // Default path
        "/" get {
            transaction {
                val openTickets = TicketsTable.select { TicketsTable.status eq 0 }.map { it.asTicket }
                val workingOnTickets = TicketsTable.select { TicketsTable.status eq 1 }.map { it.asTicket }
                val closedTickets = TicketsTable.select { TicketsTable.status eq 2 }.map { it.asTicket }

                val data = arrayOf(
                        "title" to "Overview",
                        "style" to styles["page"],
                        "openTickets" to openTickets.joinToString(""),
                        "workingTickets" to workingOnTickets.joinToString(""),
                        "closedTickets" to closedTickets.joinToString("")
                )
                renderView("index", *data)
            }

        }

        // Export path
        "/export" get {
            transaction {
                result(gson.toJson(TicketsTable.selectAll().map { it.asTicket }))
            }
        }

        // New ticket creation
        "/new" post {
            val newObject = gson.fromJson(body(), NewRequestObject::class.java)
            transaction {
                TicketsTable.insert {
                    it[title] = newObject.title
                    it[description] = newObject.description
                    it[status] = 0
                }
            }
        }

        // Update request
        "/update" post {
            transaction {
                val obj: UpdateRequestObject = gson.fromJson(body(), UpdateRequestObject::class.java)
                TicketsTable.update({ TicketsTable.id eq obj.id }) { it[status] = obj.status }
            }
        }
    }

    /**
     * Returns Ticket object from result row...
     */
    private val ResultRow.asTicket get() = Ticket(this[TicketsTable.id], this[TicketsTable.title], this[TicketsTable.description], this[TicketsTable.status])
    private infix fun String.get(block: Context.() -> Unit) = app.get(this, block)
    private infix fun String.post(block: Context.() -> Unit) = app.post(this, block)
}
