# TicketSytem-MVC

## Preview:
[Imgur](https://imgur.com/nsDySwj)

## Live:
Ask for the live url...

## MVC Framework
Developed by me, uses html templates set in *views* and style in *styles*.

HTML templates have variables stored as: `@myVariable`

## Libs:
    Backend:
        - Javalin (http/s request handling)
        - Exposed by JetBrains (Database integration)
        - Gson by Google (Json parsing)
    
    Frontend:
        - jQuery
        
